# Les différents types de manuscrits hagiographiques selon Ehrhard

## But
Le but de ce projet est de proposer des notes sur les différentes catégories de manuscrits hagiographiques proposées par Ehrhard dans les 3 volumes de ses *Überlieferung und bestand der hagiographischen und homiletischen literatur der grieschichen kirche*. Il s'agit donc essentiellement d'une traduction des introductions

## Comment participer
1. Cloner le projet. 
2. Éditer le fichier qui vous intéresse, selon la [syntaxe Markdown](http://fr.wikipedia.org/wiki/Markdown).
Un dossier par "Abschnitt", un fichier par grand type de manuscrit (correspondant à des § dans le plan). 
3. Proposer ensuite une pull request pour envoyer cela dans le dépôt principal.