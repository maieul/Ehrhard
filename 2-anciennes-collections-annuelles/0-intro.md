#Les anciennes collections annuelles (I, 154)

De la  masse des manuscrits hagiographiques en minuscules ressort un premier groupe, auquel appartient le prestigieux Code. Paris. Grec. 1470 sus-mentionné et qui représente ces Πανηγυρικομαρτυρολόγια, 
ainsi que Méthode de Constantinople a nommé sa collection.
Leur trait caractéristique  se situe dans l'égal répartition entre  les fêtes fixes et les fêtes de l'année liturgique mobile.
Je l'appel donc "collections annuelles", afin de pouvoir les distinguer nettement des ménologes d'une part et des panégyriques d'autre part.

Le groupe de manuscrits correspondant peut se diviser en deux sous groupes.
Par comparaison des manuscrits, leurs trait caractéristique apparaît, il s'est avéré que l'un porte sur l'ensemble de l'année liturgique, tandis que l'autre porte sur chacune des moitiés de l'année, ce qui s'applique également à la collection de méthode.
